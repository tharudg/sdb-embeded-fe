import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContComponent } from './main-cont.component';

describe('MainContComponent', () => {
  let component: MainContComponent;
  let fixture: ComponentFixture<MainContComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainContComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
