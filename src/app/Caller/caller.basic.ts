
import Axios from 'axios';
import { Injectable } from '@angular/core';
import { CallerPath } from './caller.path';
import { Router } from '@angular/router';

@Injectable()
export class BaseCaller {

  public axios = Axios;
  public callerPath = new CallerPath();

  constructor(
      // public authStore: AuthStore,
      public router: Router
  ) {
  }

  /**
   * @param {string} path
   * @param {string} data
   * @returns
   * @memberof BaseService
   */
  get(path, data) {
    return new Promise((resolve, reject) => {
      this.axios
        .get(path, { params: data })
        .then(result => resolve(result.data))
        .catch(err => reject(err));
    });
  }
  
 /**
   * @param {string} path
   * @param {string} data
   * @returns
   * @memberof BaseService
   */

  post(path, data) {
    return new Promise((resolve, reject) => {
      this.axios
        .post(path, data)
        .then(result => resolve(result.data))
        .catch(err => reject(err));
    });
  }

  /**
   * @param {string} path
   * @param {string} data
   * @returns
   * @memberof BaseService
   */

  delete(path, data) {
    return new Promise((resolve, reject) => {
      this.axios
        .delete(path, data)
        .then(result => resolve(result.data.data))
        .catch(err => reject(err));
    });
  }


}
