
export class CallerPath {
  ip = '192.168.43.120'
  raspi = 'raspberrypi'
  prodBase  = `http://${this.ip}/api`;
  local =  'http://localhost:5000';
  base =  this.prodBase;
  
  image = this.base + '/scanned/image';
  points = this.base + '/points'
  delPoints = (id)=>[this.base + `/points/${id}`];
}

