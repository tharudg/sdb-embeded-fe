import { TestBed } from '@angular/core/testing';

import { MainStoreService } from './main-store.service';

describe('MainStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MainStoreService = TestBed.get(MainStoreService);
    expect(service).toBeTruthy();
  });
});
