import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import {SuiModule} from 'ng2-semantic-ui';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseCaller } from './Caller/caller.basic';
import { CallerPath } from './Caller/caller.path';
import { NodeCaller } from './Caller/nodeCaller';
import { MainContComponent } from './main-cont/main-cont.component';
import { MainStoreService } from './store/main-store.service';

@NgModule({
  declarations: [
    AppComponent,
    MainContComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SuiModule
  ],
  providers: [
    BaseCaller,
    CallerPath,
    NodeCaller,
    MainStoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
