import { Component, OnInit } from '@angular/core';
import { NodeCaller } from '../Caller/nodeCaller';
import { observable, Observable } from 'rxjs';
import { MainStoreService } from '../store/main-store.service';
import { CallerPath } from '../Caller/caller.path';

@Component({
  selector: 'app-main-cont',
  templateUrl: './main-cont.component.html',
  styleUrls: ['./main-cont.component.css']
})
export class MainContComponent implements OnInit {
 
  headElements = ['ID', 'X position', 'Y position'];
  scannedImage = '';
  selectedPoints:Point[] = [];
  isUploading:boolean = false;
  isSoldering:boolean = false;
  socket: any;
  y0 = 0;
  currentangle:number=0;
  resServer;
  
  currentPosition: Point = {
    x:0,
    y:0
  }


  constructor(
    public call:NodeCaller,
    public mainStore:MainStoreService,
    public pathCaller:CallerPath
  ) {}


  ngOnInit() {
    
  }


  getImage(){
    this.isUploading =true;
    this.selectedPoints=[];
    // this.scannedImage = `https://www.aat-corp.com/wp-content/uploads/2019/06/pcb.jpg`;
    this.call.getImage().then((data)=>{
      let res = data.toString().split("/", 10);
      this.scannedImage = `http://${this.pathCaller.ip}/assets/scanned/${res['7']}`;
      
      console.log(res);
      
      setTimeout(()=>{
        this.isUploading = false
      },1000)
      // this.ngOnInit();
    })
    .catch((e)=>{alert(e)});
    this.isUploading=false;
  }


  onMouse(e){
    this.currentPosition = {
      x: e.offsetX+10,
      y: e.offsetY-5
    } 
  }

  onClikPoint(){
    if(this.scannedImage !=''){this.selectedPoints.push(this.currentPosition);}
  }


  onRemove(index: number){
    this.selectedPoints.splice(index, 1);
  }


 async solderPoints(){
    this.mainStore.isSoldering = true
    console.log(this.selectedPoints);
    let len ;
    let selectedPoint:SoldPoint ={angleStep :0, length:0};

    for (let i = 0; i < this.selectedPoints.length; i++) {
      len = i;
      console.log("for i : "+i);

      selectedPoint.angleStep =await this.calculateAngle(this.selectedPoints[i].x,this.selectedPoints[i].y);
      selectedPoint.length = await this.calculateLength(this.selectedPoints[i].x,this.selectedPoints[i].y);
      let data = await this.call.setPoints(selectedPoint)
      this.sleepDelay(4000+selectedPoint.angleStep*100)
      console.log(data)
      if(len >= this.selectedPoints.length-1){
        this.mainStore.isSoldering = false;
      }
    }


  }


  abort(){
    this.mainStore.isSoldering = false;
  }

  
  async calculateLength(x,y){
   
    let y1=500-y+this.y0;
    let x1:number = await this.getX(x)
   
    length = Math.sqrt((y1)^2+ (x1)^2);
    console.log(x,y)
    length = (length-3) *25/22;

    console.log("lenght :  ",length)
    return Math.round(length);
  }

  async getX(x){

    if(x<400){
      return 400-x;
    }
    else{
     return x-400;
    }

  }
  async calculateAngle(x,y){

    let y1=500-y+this.y0;
    let x1:number = await this.getX(x);
    let a :number = -1;
    let scaleCons = 500; 
    
    if(x<400){
      x1 = 400-x;
      a= 1;
      scaleCons =300;
    }
    else{
      x1 = x-400;
    }
    let theta = (Math.atan(x1/y1)* 180 / Math.PI)*a;
    console.log("angle : ",theta);

    theta = theta * scaleCons / 72;

    console.log("Scaled angle : ",theta);
    return Math.round(theta) ;
  }
  
sleepDelay(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
  }

}
 
class Point {

  x:number;
  y:number;
}


class SoldPoint{
  length:number;
  angleStep:number;
}
